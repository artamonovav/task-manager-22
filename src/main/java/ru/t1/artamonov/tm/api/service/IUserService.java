package ru.t1.artamonov.tm.api.service;

import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findOneById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
