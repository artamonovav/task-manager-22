package ru.t1.artamonov.tm.api.repository;

import ru.t1.artamonov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    M add(M model);

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    Integer getSize();

    M remove(M model);

    void removeAll(Collection<M> collection);

    M removeById(String id);

    M removeByIndex(Integer index);

}
