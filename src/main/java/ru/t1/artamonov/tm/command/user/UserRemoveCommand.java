package ru.t1.artamonov.tm.command.user;

import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    private static final String NAME = "user-remove";

    private static final String DESCRIPTION = "user remove";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
