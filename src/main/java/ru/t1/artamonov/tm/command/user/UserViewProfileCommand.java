package ru.t1.artamonov.tm.command.user;

import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    private static final String NAME = "view-user-profile";

    private static final String DESCRIPTION = "view profile of current user";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
